const path = require("path");
const { node2ForestPage, createNodeManifest, loadForestPages } = require("./page-functions");

const ForestNodeType = "ForestNode";

let options = { }


exports.pluginOptionsSchema = ({ Joi }) => {
    return Joi.object({
        location: Joi.string().required()
    })
}

exports.onPreInit = (_, pluginOptions) => {
    options = pluginOptions;
}

exports.sourceNodes = async ({ actions, createContentDigest }) => {

    const { unstable_createNodeManifest, createNode } = actions;

    const nodeData = {
        title: "Forest Node",
        description: "A node representing a forest template",
    };

    var forestPages = loadForestPages(options.location);
    for (const i in forestPages) {
        const forestPage = forestPages[i];
        const newNode = {
            ...nodeData,
            forestPage: forestPage,
            id: `ForestNode-${forestPage.template}`,
            internal: {
                type: ForestNodeType,
                contentDigest: createContentDigest(forestPage),
                content: JSON.stringify(forestPage)
            },
        }
        var createdNode = createNode(newNode);
        createNodeManifest({
            entryItem: forestPage,
            entryNode: createdNode,
            createNodeManifest: unstable_createNodeManifest
        });
    }
}
exports.onCreateNode = ({ node, actions }) => {
    console.log(`onCreateNode: ${node.internal.type}`);
    if (node.internal.type === ForestNodeType) {
        console.log(`onCreateNode: ${JSON.stringify(node)}`);
    }
}

exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const { data } = await graphql(
        `query {
            allForestNode {
                edges {
                    node {
                        id
                        internal {
                            type
                            content
                        }
                    }
                }
            }
        }`);
        
    data.allForestNode.edges.forEach(({ node }) => {
        const componentPath = path.resolve(`${__dirname}/forest-page.tsx`);
        createPage(node2ForestPage(node, componentPath));
    });
}