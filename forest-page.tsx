import React, { FC, memo, useEffect } from "react";
import { DefaultForestHooks, ForestApp, ForestHooksContext, useNavigate } from '@vdimensions/forest-js-react';
import { ForestGatsbyClient } from "./client";
import "/src/forest/views";

type GatsbyNavigatorProps = {
    template: string
}

const GatsbyNavigatorInner : FC<GatsbyNavigatorProps> = memo((props) => {
    const navigate = useNavigate();
    //
    // Necessary to use effect because of endless loop
    //
    useEffect(() => {
        navigate(props.template);
    }, [props.template]);
    
    return (<>{props.children}</>);
});

const ForestPage = (props) => {
    const {template, data} = props.pageContext;
    
    const GatsbyNavigator : FC<any> = memo((props) => {

        return (
            <ForestHooksContext.Provider value={DefaultForestHooks}>
                <GatsbyNavigatorInner template={template}>
                    {props.children}
                </GatsbyNavigatorInner>
            </ForestHooksContext.Provider>
        );
    });

    return (
        <ForestApp 
            loadingIndicator={<span>LOADING</span>}
            client={new ForestGatsbyClient(data)}
            navigator={GatsbyNavigator}
            />
    );
};

export default ForestPage;